<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use yii\base\Exception;
use yii\db\Connection;
use yii\db\Query;


/**
 * Class MemTableBehaviors
 * @package common\behaviors
 *
 * Example to usage:
 * In model:
 * public function behaviors()
 * {
 *    return [
 *       'MemTable' => [
 *            'class' => MemTableBehaviors::class,
 *             'prefix' => 'cached_table'
 *        ],
 * }
 *
 * In controller:
 * (new Model())->getFieldsByName('name');
 * (new Model())->getFieldsBySQL('SELECT name FROM table');
 */
class MemTableBehaviors extends Behavior
{

    /**
     * @var prefix for create unique name for cache
     */
    public $prefix;

    /**
     * @return array events to clear cached data
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'deleteCache',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'deleteCache',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'deleteCache',
        ];
    }

    /**
     * Clear cache data
     */
    public function deleteCache()
    {
        Yii::$app->cache->delete($this->prefix);
    }

    /**
     * @param $name of field to find data in owned table
     * @return array|mixed
     * @throws Exception
     *
     * Function checks field in table. If it exists take all data from column
     *  add data to cache ana return as array.
     */
    public function getFieldsByName($name)
    {
        $cache = Yii::$app->cache;
        $a = $cache->get($this->prefix);
        if ($a === false) {
            $tableName = $this->owner->tableName();
            $table = Yii::$app->db->schema->getTableSchema($tableName);
            if (!isset($table->columns[$name])) {
                throw new Exception('Field not found');
            }
            $a = (new Query())
                ->select($name)
                ->from($tableName)
                ->column();

            $cache->set($this->prefix, $a);
        }
        return $a;
    }

    /**
     * @param $request SQL to get a data
     * @return mixed
     * @throws \Throwable
     *
     * Function create command from inner request and add result to cache
     */
    public function getFieldsBySQL($request)
    {
        $db = new Connection();
        $result = $db->cache(function ($db) use ($request) {
            return $db->createCommand($request)->queryAll();
        });

        return $result;
    }
}
