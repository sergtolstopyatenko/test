<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\web\Controller;

class MetaRegisterBehaviors extends Behavior
{

    public function init()
    {
        parent::init();
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'registerMetaTag',
        ];
    }

    /**
     * Run behaviors
     */
    public function registerMetaTag()
    {
        $this->owner->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Test metatag'
        ], 'metaDescription');
    }
}
