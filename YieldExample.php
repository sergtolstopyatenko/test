<?php

use moonland\phpexcel\Excel;

function getImportData($file)
{
    $data = Excel::import($file);
    $counter = $data ? count($data) : 0;
    $i = 1;
    while ($i <= $counter) {
        yield convertData($data[$i]);
    }
}


foreach (getImportData('shops.xls') as $row) {
    echo($row);
}

function convertData($row)
{
    $result = '';
    if (isset($row['name'])) {
        $result .= 'Имя : ' . $row['name'] . ' ';
    }
    if (isset($row['address'])) {
        $result .= 'Адрес : ' . $row['address'] . ' ';
    }

    return $result;
}

