<?php

namespace common\models\traits;

use Yii;

trait BatchTrait
{
    /**
     * @param array $fields
     * @param array $rows
     * @throws \yii\db\Exception
     * When function call need send 2 arrays: first array of fields, second array of arrays with rows data
     */
    public function saveBatchData($fields = [], $rows = [])
    {
        if (!empty($fields) && !empty($rows)) {
            $table = $this->tableName();
            $batchUpdateSql = Yii::$app->db->createCommand()->batchInsert($table, $fields, $rows)->getRawSql();
            Yii::$app->db->createCommand($batchUpdateSql . ' ON DUPLICATE KEY UPDATE')->execute();
        }
    }
}
