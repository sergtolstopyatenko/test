<?php

final class SingletonConnection
{
    /**
     * @var string the host to connection
     */
    public $host;
    /**
     * @var int port on the host
     */
    public $port;
    /**
     * @var string the login to connection
     */
    public $login;
    /**
     * @var string the password to connection
     */
    public $password;
    /**
     * @var string the amqp virtual host
     */
    public $vhost;

    /**
     * @var $amqp associated with connection.
     * This property manged by connect and disconnect methods.
     */
    public $amqp;

    /**
     * @var instance for singletone
     */
    private static $instance;

    /**
     * @return instance|SingletonConnection
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * Connect to amqp. If connection exists return this connection
     */
    public function connect()
    {
        if ($this->amqp !== null) {
            return;
        }

        try {
            $this->amqp = $this->createConnection();
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->errorInfo, (int)$e->getCode(), $e);
        }
    }

    /**
     * Disconnect to amqp.
     */
    public function disconnect()
    {
        if ($this->amqp !== null) {
            $this->amqp = null;
        }
    }

    /**
     * @return bool
     * @throws \AMQPConnectionException
     */
    protected function createConnection()
    {
        $cnn = new \AMQPConnection([
            'host' => $this->host,
            'vhost' => $this->vhost,
            'port' => $this->port,
            'login' => $this->login,
            'password' => $this->password
        ]);

        return $cnn->connect();
    }

    /**
     * prevent from creating multiple instances,
     */
    private function __construct()
    {
    }

    /**
     * prevent from clone
     */
    private function __clone()
    {
    }

    /**
     * prevent from wakeup
     */
    private function __wakeup()
    {
    }
}