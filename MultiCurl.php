<?php

//Массив ссылок для многопоточного запроса
$urls = [
    'http://womo.ua/category/business/work-career/',
    'http://womo.ua/category/business/lifehack/',
];


$multiCurl = curl_multi_init();
$channels = [];

//Для каждой из ссылок формируем curl и добавляем их в набор
foreach ($urls as $url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_multi_add_handle($multiCurl, $curl);

    $channels[$url] = $curl;
}

$active = null;
//Запускаем
do {
    $mrc = curl_multi_exec($multiCurl, $active);
} while ($mrc == CURLM_CALL_MULTI_PERFORM);

while ($active && $mrc == CURLM_OK) {
    if (curl_multi_select($multiCurl) == -1) {
        continue;
    }

    do {
        $mrc = curl_multi_exec($multiCurl, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
}
//Закрываем
foreach ($channels as $channel) {
    echo curl_multi_getcontent($channel);
    curl_multi_remove_handle($multiCurl, $channel);
}

curl_multi_close($multiCurl);